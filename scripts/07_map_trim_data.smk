#README-----------------------------------------------------------------------------------------------

# Execution command: snakemake -c 14 --snakefile 07_map_trim_data.smk
# This seventh script aligns the cleaned data against the "mm39" mouse reference genome using the bowtie2. It generates alignment files in uncompressed sam format.

#CODE-------------------------------------------------------------------------------------------------

configfile: "../config/config.yaml",

rule all:
    input:
        expand("../data/4.mapping/trimmed_{sample}_mapping.sam", sample=config["samples_compiled"]),

rule map_with_bowtie2:
    input:
        fastq1 = "../data/2.trimming/trimmed_{sample}_1_P.fastq.gz",
        fastq2 = "../data/2.trimming/trimmed_{sample}_2_P.fastq.gz"
    output:
        "../data/4.mapping/trimmed_{sample}_mapping.sam"
    params:
        index = "/home/users/shared/data/Mus_musculus/Mus_musculus_GRCm39/bowtie2/all"
    threads: 12
    resources:
        mem_mb=4000,  
        time="00:20:00"
    #log: "../log/07_map_trim_data/map_{sample}.log"
    shell:
        """
        ml bowtie2/2.3.4.3
        bowtie2 --threads {threads} \
          -x {params.index} \
          -1 {input.fastq1} \
          -2 {input.fastq2} \
          -S {output}
        """