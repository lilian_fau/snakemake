#README-----------------------------------------------------------------------------------------------

# Execution command: snakemake -c 28 --snakefile 05_qc_trim_data.smk
# This fifth script performs a quality check on the cleaned DNAse-seq data using the fastqc tool, to determine its quality after cleaning.

#CODE-------------------------------------------------------------------------------------------------

configfile: "../config/config.yaml",

rule all:
    input:
        expand("../data/3.fastqc_trimming/trimmed_{sample}_P_fastqc.html", sample=config["samples"]),

rule fastqc:
    input:
        fastq_file="../data/2.trimming/trimmed_{sample}_P.fastq.gz"
    output:
        html="../data/3.fastqc_trimming/trimmed_{sample}_P_fastqc.html",
        zip="../data/3.fastqc_trimming/trimmed_{sample}_P_fastqc.zip"
    resources:
        mem_mb=5,  
        time="00:10:00"
    threads: 4
    #log: "../log/05_qc_trim_data/{sample}.log"
    shell: 
        """
        ml java fastqc/0.11.7
        fastqc --outdir ../data/3.fastqc_trimming/ -t {threads} {input.fastq_file}
        """
