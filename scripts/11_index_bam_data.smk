#README-----------------------------------------------------------------------------------------------

# Execution command: snakemake -c 14 --snakefile 11_index_bam_data.smk
# This eleventh script uses the Picard tool to generate the indexes of the bam files needed for the counting matrix.

#CODE-------------------------------------------------------------------------------------------------

configfile: "../config/config.yaml",

rule all:
    input:
        expand("../data/4.mapping/sorted_bam/sorted_{sample}.bam.bai", sample=config["samples_compiled"]),

rule build_bam_index:
    input:
        "../data/4.mapping/sorted_bam/{sample}.bam"
    output:
        "../data/4.mapping/sorted_bam/{sample}.bam.bai"
    resources:
        mem_mb=5,  
        time="00:10:00"
    #log: "../log/11_index_bam_data/{sample}.log"
    shell:
        """
        ml java
        ml picard/2.18.25
        java -jar /opt/apps/picard-2.18.25/picard.jar BuildBamIndex I={input} O={output}
        """




