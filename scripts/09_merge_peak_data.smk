#README-----------------------------------------------------------------------------------------------

# Execution command: snakemake -c 14 --snakefile 09_merge_peak_data.smk
# This ninth script merges all the bed files from peak calling into one with the bedops tool so that it can be used later to generate our counting matrix. 

#CODE-------------------------------------------------------------------------------------------------

configfile: "../config/config.yaml",

rule all:
    input:
        "../data/6.r_analysis/all_merged.bed"

rule merge_peaks_and_sort:
    input:
        peaks_files=expand("../data/5.peak_calling/{sample}/{sample}.merge.peaks", sample=config["samples_compiled"]),
    output:
        "../data/6.r_analysis/all_merged.bed"
    conda:
        "envs/08_peak_trim_data.yaml"
    resources:
        mem_mb=10,  
        time="00:10:00"
    #log: "../log/09_merge_peak_data/merge_peak.log"
    shell:
        """
        eval "$(conda shell.bash hook)"
        conda activate peak_calling
        bedops -u {input.peaks_files} > {output}
        sort -k1,1 -k2,2n -o {output} {output}
        """