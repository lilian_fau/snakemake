#README-----------------------------------------------------------------------------------------------

# Execution command: snakemake -c 28 --snakefile 02_qc_raw_data.smk
# This second script performs a quality control of raw DNAse-seq data using the fastqc tool, in order to determine their initial quality before cleaning.

#CODE-------------------------------------------------------------------------------------------------

configfile: "../config/config.yaml",

rule all:
    input:
        expand("../data/1.fastqc_raw/{sample}_fastqc.html", sample=config["samples"]),

rule fastqc:
    input:
        fastq_file="/home/users/shared/data/stategra/dna-seq/{sample}.fastq.gz"
    output:
        html="../data/1.fastqc_raw/{sample}_fastqc.html",
        zip="../data/1.fastqc_raw/{sample}_fastqc.zip"
    resources:
        mem_mb=5,  
        time="00:10:00"
    threads: 4
    shell: 
        """
        ml java fastqc/0.11.7
        fastqc --outdir ../data/1.fastqc_raw/ -t {threads} {input.fastq_file}
        """
