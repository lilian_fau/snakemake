#README-----------------------------------------------------------------------------------------------

# Execution command: snakemake -c 1 --snakefile 03_multiqc_raw_data.smk
# This third script merges the DNAse-seq raw data quality control files with the multiqc tool, to get a better view of the initial data set before cleaning.

#CODE-------------------------------------------------------------------------------------------------

configfile: "../config/config.yaml",

rule all:
    input:
        "../results/multiqc_raw.html"

rule multiqc_raw:
    input:
        fastqc_zip=expand("../data/1.fastqc_raw/{sample}_fastqc.zip", sample=config["samples"]),
    output:
        html="../results/multiqc_raw.html" 
    resources:
        mem_mb=5,  
        time="00:10:00"
    #log: "../log/03_multiqc_raw_data/multiqc.log"
    shell: 
        """
        ml gcc/8.1.0 python/3.7.1 MultiQC/1.7 
        multiqc {input.fastqc_zip} -n multiqc_raw -o ../results/
        """