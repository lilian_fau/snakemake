#README-----------------------------------------------------------------------------------------------

# Execution command: snakemake -c 1 --snakefile 13_r_analysis_data.smk
# This third script performs the various statistical processes on the counting matrix using R and the NOIseq package. 
# In this script we manually install the R package "Noiseq" from a temporary library because we don't have the write permissions to install them directly on the server. 
# To do this, we redirect the installation and execution of the library using the command: R_LIBS_USER="/home/users/student01/snakemake/tmp/".

#CODE-------------------------------------------------------------------------------------------------

rule all:
    input:
        "../data/6.r_analysis/STAT_DNaseSeq_homer_RPKM_TMM_ARSyN.txt"

rule install_dependencies:
    output:
        "../data/6.r_analysis/STAT_DNaseSeq_homer_RPKM_TMM_ARSyN.txt"
    resources:
        mem_mb=500,  
        time="00:10:00"
    #log: "../log/13_r_analysis_data/r_analysis.log"   
    shell:
        """
        ml gcc/8.1.0 R/4.0.2
        mkdir -p ../tmp
        R_LIBS_USER="../tmp/" Rscript -e 'install.packages("BiocManager", lib="../tmp/", repos="https://cran.rstudio.com/"); BiocManager::install("NOISeq", lib="../tmp/", dependencies = TRUE, force = TRUE)'
        R_LIBS_USER="../tmp/" Rscript -e 'source("analysis.R")'
        rm -r ../tmp
        cp ../data/6.r_analysis/STAT_DNaseSeq_homer_RPKM_TMM_ARSyN.txt ../results/
        """

