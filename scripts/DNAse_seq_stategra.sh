#!/bin/bash

#README-----------------------------------------------------------------------------------------------

# Execution command: bash ./DNAse_seq_stategra.sh

#CODE-------------------------------------------------------------------------------------------------

# Modules
ml gcc/8.1.0 python/3.7.1 snakemake

#  Snakemake scripts execution

echo -e "\n#ROOT --------------------------------------------------------------------------------------------------\n"
snakemake --cores 1 --jobs 1 --snakefile 01_root.smk --use-conda --cluster "sbatch --mem {resources.mem_mb} --cpus-per-task {threads} --time={resources.time}"


echo -e "\n#QC RAW DATA -------------------------------------------------------------------------------------------\n"
snakemake --cores 28 --jobs 28 --snakefile 02_qc_raw_data.smk --use-conda --cluster "sbatch --mem {resources.mem_mb} --cpus-per-task {threads} --time={resources.time} --output=../log/02_qc_raw_data/{wildcards.sample}_slurm-%j.log"


echo -e "\n#MUTIQC RAW DATA ---------------------------------------------------------------------------------------\n"
snakemake --cores 1 --jobs 1 --snakefile 03_multiqc_raw_data.smk --use-conda --cluster "sbatch --mem {resources.mem_mb} --cpus-per-task {threads} --time={resources.time} --output=../log/03_multiqc_raw_data/multiqc_slurm-%j.log"


echo -e "\n#TRIM RAW DATA -----------------------------------------------------------------------------------------\n"
snakemake --cores 14 --jobs 14 --snakefile 04_trim_raw_data.smk --use-conda --cluster "sbatch --mem {resources.mem_mb} --cpus-per-task {threads} --time={resources.time} --output=../log/04_trim_raw_data/trimming_{wildcards.sample}_slurm-%j.log"


echo -e "\n#QC TRIM DATA ------------------------------------------------------------------------------------------\n"
snakemake --cores 28 --jobs 28 --snakefile 05_qc_trim_data.smk --use-conda --cluster "sbatch --mem {resources.mem_mb} --cpus-per-task {threads} --time={resources.time} --output=../log/05_qc_trim_data/{wildcards.sample}_slurm-%j.log"

echo -e "\n#MUTIQC TRIM DATA --------------------------------------------------------------------------------------\n"
snakemake --cores 1 --jobs 1 --snakefile 06_multiqc_trim_data.smk --use-conda --cluster "sbatch --mem {resources.mem_mb} --cpus-per-task {threads} --time={resources.time} --output=../log/06_multiqc_trim_data/multiqc.log"


echo -e "\n#MAPPING -----------------------------------------------------------------------------------------------\n"
snakemake --cores 14 --jobs 14 --snakefile 07_map_trim_data.smk --use-conda --cluster "sbatch --mem {resources.mem_mb} --cpus-per-task {threads} --time={resources.time} --output=../log/07_map_trim_data/map_{wildcards.sample}_slurm-%j.log"


echo -e "\n#PEAK --------------------------------------------------------------------------------------------------\n"
snakemake --cores 14 --jobs 14 --snakefile 08_peak_trim_data.smk --use-conda --cluster "sbatch --mem {resources.mem_mb} --cpus-per-task {threads} --time={resources.time} --output=../log/08_peak_trim_data/peak_{wildcards.sample}_slurm-%j.log"


echo -e "\n#MERGE PEAK --------------------------------------------------------------------------------------------\n"
snakemake --cores 14 --jobs 14 --snakefile 09_merge_peak_data.smk --use-conda --cluster "sbatch --mem {resources.mem_mb} --cpus-per-task {threads} --time={resources.time} --output=../log/09_merge_peak_data/merge_peak_slurm-%j.log"


echo -e "\n#SAM TO BAM --------------------------------------------------------------------------------------------\n"
snakemake --cores 14 --jobs 14 --snakefile 10_sam_to_bam_data.smk --use-conda --cluster "sbatch --mem {resources.mem_mb} --cpus-per-task {threads} --time={resources.time} --output=../log/10_sam_to_bam_data/{wildcards.sample}_slurm-%j.log"


echo -e "\n#INDEX BAM ---------------------------------------------------------------------------------------------\n"
snakemake --cores 14 --jobs 14 --snakefile 11_index_bam_data.smk --use-conda --cluster "sbatch --mem {resources.mem_mb} --cpus-per-task {threads} --time={resources.time} --output=../log/11_index_bam_data/{wildcards.sample}_slurm-%j.log"


echo -e "\n#MATRIX ------------------------------------------------------------------------------------------------\n"
snakemake --cores 1 --jobs 1 --snakefile 12_count_matrix_data.smk --use-conda --cluster "sbatch --mem {resources.mem_mb} --cpus-per-task {threads} --time={resources.time} --output=../log/12_count_matrix_data/matrix_count_slurm-%j.log"


echo -e "\n#R ANALYSIS --------------------------------------------------------------------------------------------\n"
snakemake --cores 1 --jobs 1 --snakefile 13_r_analysis_data.smk --use-conda --cluster "sbatch --mem {resources.mem_mb} --cpus-per-task {threads} --time={resources.time} --output=../log/13_r_analysis_data/r_analysis_slurm-%j.log"

echo -e "\n#TRAITEMENT TERMINE ------------------------------------------------------------------------------------\n"
