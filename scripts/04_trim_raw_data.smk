#README-----------------------------------------------------------------------------------------------

# Execution command: snakemake -c 28 --snakefile 04_trim_raw_data.smk
# This fourth step cleans the raw DNAse-seq data using the trimmomatic tool to remove adapters and poly-G tails.

#CODE-------------------------------------------------------------------------------------------------

configfile: "../config/config.yaml",

rule all:
    input:
        expand("../data/2.trimming/trimmed_{sample}_1_P.fastq.gz", sample=config["samples_compiled"]),
        expand("../data/2.trimming/trimmed_{sample}_2_P.fastq.gz", sample=config["samples_compiled"]),

rule trim_raw_data:
    input:
        forward = "/home/users/shared/data/stategra/dna-seq/{sample}_1.fastq.gz",
        reverse_input = "/home/users/shared/data/stategra/dna-seq/{sample}_2.fastq.gz"
    output:
        forward_paired = "../data/2.trimming/trimmed_{sample}_1_P.fastq.gz",
        forward_unpaired = "../data/2.trimming/trimmed_{sample}_1_U.fastq.gz",
        reverse_paired = "../data/2.trimming/trimmed_{sample}_2_P.fastq.gz",
        reverse_unpaired = "../data/2.trimming/trimmed_{sample}_2_U.fastq.gz",
    params:
        adapter_file = "adapters/TruSeq3-PE-2.fa"
    resources:
        mem_mb=1500,  
        time="00:10:00"
    threads:
        8
    conda:
        "envs/04_trim_raw_data.yaml"
    shell:
        """
        ml java
        java -jar /opt/apps/trimmomatic-0.38/trimmomatic-0.38.jar PE \
            -threads {threads} \
            {input.forward} \
            {input.reverse_input} \
            {output.forward_paired} \
            {output.forward_unpaired} \
            {output.reverse_paired} \
            {output.reverse_unpaired} \
            ILLUMINACLIP:{params.adapter_file}:2:30:10 \
            LEADING:5 TRAILING:5 SLIDINGWINDOW:4:5 MINLEN:25
        """




