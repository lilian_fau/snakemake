#README-----------------------------------------------------------------------------------------------

# Execution command: snakemake -c 1 --snakefile 01_root.smk
# The aim of this first script is to create the general root of the script so that it can run normally and deposit all the data generated in a clear data tree.

#CODE-------------------------------------------------------------------------------------------------

rule create_root:
    resources:
        mem_mb=5,  
        time="00:10:00"
    threads: 1
    #log: "../log/01_root_data/root_data.log"
    shell:
        """
        mkdir -p ../data ../log ../scripts ../results
        mkdir -p ../data/1.fastqc_raw ../data/2.trimming ../data/3.fastqc_trimming ../data/4.mapping ../data/5.peak_calling ../data/6.r_analysis
        mkdir -p ../log/01_root_data ../log/02_qc_raw_data ../log/03_multiqc_raw_data ../log/04_trim_raw_data ../log/05_qc_trim_data ../log/06_multiqc_trim_data ../log/07_map_trim_data ../log/08_peak_trim_data ../log/09_merge_peak_data ../log/10_sam_to_bam_data ../log/11_index_bam_data ../log/12_count_matrix_data ../log/13_r_analysis_data
        chmod 770 ../data ../log ../scripts ../results
        ln -s /home/users/shared/data/stategra/dna-seq/ ../data/dna_seq
        ln -s /home/users/shared/data/Mus_musculus/Mus_musculus_GRCm39/ ../data/mus_musculus
        echo -e "Root generation completed\\n"
        """


