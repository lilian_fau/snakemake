![UCA logo](https://www.uca.fr/medias/photo/logo-uca-long-300dpi_1493730258077-png)

# UE - Calculs parallèles et Programmation GPU

#### Author: Lilian Faurie - Master 2 Bioinformatique
#### Enseignante: Goué Nadia
#### Date: 30/12/2023

The project consists of re-analysing, on a [UCA Mesocentre](https://mesocentre.uca.fr/) computing cluster, the data produced in the publication by *[Gomez-Cabrero et al. (2019)](https://www.nature.com/articles/s41597-019-0202-7)*. In the case of this snakemake pipeline, we have been assigned subsets of data from DNase-seq.

The snakemake pipeline created for the occasion is available on the project's [GitLab](https://gitlab.com/lilian_fau/snakemake/). The DNAse-seq STATEGRA data used for the analyses are available here in free access on the [NCBI website](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE75390). 

![enter image description here](https://media.springernature.com/lw685/springer-static/image/art%3A10.1038%2Fs41597-019-0202-7/MediaObjects/41597_2019_202_Fig4_HTML.png?as=webp)

# Structure️ 📂

**The structure that the snakemake pipeline will generate once the analysis has been completed consists of four main folders :**

```
├── data
│   ├── 1.fastqc_raw
│   ├── 2.trimming
│   ├── 3.fastqc_trimming
│   ├── 4.mapping
│   ├── 5.peak_calling
│   └── 6.r_analysis
│
├── scripts
│   ├── envs
│   │ 	 ├── 04_trim_raw_data.yaml
│   │ 	 └── 08_peak_trim_data.yaml
│   │
│   ├── 01_root.smk
│   ├── 02_qc_raw_data.smk
│   ├── 03_multiqc_raw_data.smk
│   ├── 04_trim_raw_data.smk
│   ├── 05_qc_trim_data.smk
│   ├── 06_multiqc_trim_data.smk
│   ├── 07_map_trim_data.smk
│   ├── 08_peak_trim_data.smk
│   ├── 09_merge_peak_data.smk
│   ├── 10_sam_to_bam_data.smk
│   ├── 11_index_bam_data.smk
│   ├── 12_count_matrix_data.smk
│   ├── 13_r_analysis_data.smk
│   ├── analysis.R
│   └── DNAse_seq_stategra.sh
├── config
│   └── config.yaml
│ 
├── log
└── results
```
The "***data***" directory comprises a series of sub-directories numbered in the order of data processing, containing all the data generated throughout the analysis.

The "***results***" directory hosts the main files resulting from the analyses.

The '***scripts***' directory contains all the processing carried out during the analysis. These snakemake scripts run in parallel under the HPC architecture.

The '***config***' directory contains the configuration of the data names required by the various scripts for their analyses.

Finally, the "***log***" directory contains all the logs for the various stages of processing carried out during the analysis.

# Execution 📝

**How do I start analysing the data?** 
Once the project has been cloned from our GitLab page, simply run the bash script named "**DNAse_seq_stategra.sh**" from the "scripts" folder. This will allow all snakemake processing pipelines to be executed. To do this, you need a bash command:

> **`bash DNAse_seq_stategra.sh`**

The whole process may **take several minutes to complete**.

# Prerequisites ⚙️

The **following tools are required** to run the pipeline, so it is essential that they are available on the HPC :

> - bedtools/2.27.1
> - bowtie2/2.3.4.3
> - conda/4.12.0
> - fastqc/0.11.7
> - gcc/8.1.0
> - java
> - MultiQC/1.7
> - picard/2.18.25
> - python/3.7.1
> - R/4.0.2
> - trimmomatic/0.38

Once installed, they can be loaded using the slurm command :

> `ml "software name"`


